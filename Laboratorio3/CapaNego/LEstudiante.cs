﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEnt;

namespace CapaNego
{
    public class LEstudiante
    {
    
       private Estudiante Persona { get; set; }
        LinkedList<Estudiante> estudiante = new LinkedList<Estudiante>();


        public LEstudiante()
        {
            Persona = new Estudiante();
        }

        public void insertarEstudiante(string codEstudiante, int cedula, string nombre, string apellido, string carrera, string materia, int nota, string estado)

        {
            Persona.CodEstudiante = codEstudiante;
            Persona.Cedula = cedula;
            Persona.Nombre = nombre;
            Persona.Apellido = apellido;
            Persona.Carrera = carrera;
            Persona.Materia = materia;
            Persona.Nota = nota;
            Persona.Estado = estado;

            
            estudiante.AddLast(Persona);

        }

        public string cargarEstudiante()
        {
            string estudiantes = "";
            foreach (var c in estudiante)
            {
                estudiantes += c.ToString() + Environment.NewLine;
            }
            return estudiantes;
        }
    }
}