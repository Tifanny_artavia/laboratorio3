﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEnt
{
    public class Estudiante : Persona
    {
        public string Carrera { get; set; }
        public string Materia { get; set; }
        public int Nota { get; set; }
        public string Estado { get; set; }


        public Estudiante() : base()
        {

        }
        public Estudiante( string codEstudiante, int cedula, string nombre, string apellido, string carrera, string materia, int nota, string estado)
            : base(cedula, nombre, apellido, codEstudiante)
        {
            Carrera = carrera;
            Materia = materia;
            Nota = nota;
            Estado = estado;
        }
    }
}
