﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEnt
{
    public abstract class Persona
    {
        public int Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string CodEstudiante { get; set; }

        public Persona()
        {
        }

        public Persona(int cedula, string nombre, string apellido, string codEstudiante)
        {
            Cedula = cedula;
            Nombre = nombre;
            Apellido = apellido;
            CodEstudiante = codEstudiante;
        }

        public override string ToString()
        {
            return Nombre + "" + Cedula + "" + Apellido + "" + CodEstudiante + "";
        }
    }
}
