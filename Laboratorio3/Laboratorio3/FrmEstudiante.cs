﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNego; 



namespace Laboratorio3
{
    public partial class FrmEstudiante : Form
    {
        LEstudiante estudiante = new LEstudiante();
        int cantfilas;
        public FrmEstudiante()
        {
            InitializeComponent();
            limpiar();
        }

        

        private void FrmEstudiante_Load(object sender, EventArgs e)
        {

        }
        
        public void limpiar()
        {
            txtCedula.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtCodEstud.Text = "";
            cboCarrera.Text = "";
            cboMateria.Text = "";
            txtNota.Text = "";
            cboEstado.Text = "";


        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            dgvEstudiante.Rows.Add();
            cantfilas = dgvEstudiante.Rows.Count-1;

            dgvEstudiante[0, cantfilas].Value = txtCedula.Text;
            dgvEstudiante[1, cantfilas].Value = txtNombre.Text;
            dgvEstudiante[2, cantfilas].Value = txtApellido.Text;
            dgvEstudiante[3, cantfilas].Value = txtCodEstud.Text;
            dgvEstudiante[4, cantfilas].Value = cboCarrera.Text;
            dgvEstudiante[5, cantfilas].Value = cboMateria.Text;
            dgvEstudiante[6, cantfilas].Value = txtNota.Text;
            dgvEstudiante[7, cantfilas].Value = cboEstado.Text;
            limpiar();
            FrmException.Notificacion("Datos Almacenados ");


        }



        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string dato = cboEstado.Text.ToString();

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            dgvEstudiante.Rows.Remove(dgvEstudiante.CurrentRow);
            FrmException.Notificacion("Datos Eliminados ");

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            int pos = dgvEstudiante.CurrentRow.Index;

            string est;
            est=txtCedula.Text;
            est = txtNombre.Text;
            est = txtApellido.Text;
            est = txtCodEstud.Text;
            est = cboCarrera.Text;
            est = cboMateria.Text;
            est = txtNota.Text;
            est = cboEstado.Text;

            dgvEstudiante[0, pos].Value = txtCedula.Text;
            dgvEstudiante[1, pos].Value = txtNombre.Text;
            dgvEstudiante[2, pos].Value = txtApellido.Text;
            dgvEstudiante[3, pos].Value = txtCodEstud.Text;
            dgvEstudiante[4, pos].Value = cboCarrera.Text;
            dgvEstudiante[5, pos].Value = cboMateria.Text;
            dgvEstudiante[6, pos].Value = int.Parse(txtNota.Text);
            dgvEstudiante[7, pos].Value = cboEstado.Text;

            limpiar();
            FrmException.Notificacion("Datos Editados ");

        }

        private void dgvEstudiante_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvEstudiante_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }
    }
}
