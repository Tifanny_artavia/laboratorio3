﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio3
{
    public partial class FrmException : Form
    {
        public FrmException()
        {
            InitializeComponent();
        }

        public FrmException(string mensaje)
        {
            InitializeComponent();
            lblMensaje.Text = mensaje;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        public static void Notificacion(string mensaje)
        {
            FrmException frm = new FrmException(mensaje);
            frm.ShowDialog();
        }
        private void FrmException_Load(object sender, EventArgs e)
        {

        }
    }
}
