﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Estudiante : Persona
    {
        public int Id { get; set; }
        public string Carrera { get; set; }
        public string Materia { get; set; }
        public int Nota { get; set; }
        public string Estado { get; set; }


        public Estudiante() : base()
        {

        }
        public Estudiante(int id, string codEstudiante, int cedula, string nombre,string apellido, string carrera, string materia, int nota, string estado) 
            : base(cedula, nombre, apellido,codEstudiante)
        {
            Id = id;
            Carrera = carrera;
            Materia = materia;
            Nota = nota;
            Estado = estado;
        }
    }
}

