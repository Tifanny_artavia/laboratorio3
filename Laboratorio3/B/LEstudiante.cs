﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;

namespace CapaNegocio
{
    public class LEstudiante
    {
            private Estudiante Persona { get; set; }
            LinkedList<Estudiante> estudiante = new LinkedList<Estudiante>();

            public LEstudiante()
            {
                Persona = new Estudiante();
            }

            public void insertarEstudiante(string codEstudiante, int cedula, string nombre, string apellido, string carrera, string materia, int nota, string estado)

            {
                Persona.Cedula = cedula;
                Persona.Nombre = nombre;
                Persona.Apellido = apellido;
                Persona.CodEstudiante = codEstudiante;

                ///*Cargar datos por composición*/

                //Persona.OTarjeta.LimiteSaldo = limiteSaldo;
                //Persona.OTarjeta.NumeroTerjeta = numeroTarjeta;
                //Persona.OTarjeta.Saldo = saldoTarjeta;
                //Persona.OTarjeta.Tipo = tipoTarjeta;

                ///*agregar la agencia por metodo de agregación*/

                //Agencia sucu = new Agencia();
                //sucu.Codigo = codAgencia;
                //sucu.Nombre = nomAgencia;
                //sucu.asesor.Cedula = cedulaAsesor;
                //sucu.asesor.Nombre = nombreAsesor;
                //sucu.asesor.Edad = edadAsesor;
                //Persona.OAgencia = sucu;
                estudiante.AddLast(Persona);

            }

            public string cargarEstudiante()
            {
                string estudiantes = "";
                foreach (var c in estudiante)
                {
                    estudiantes += c.ToString() + Environment.NewLine;
                }
                return estudiantes;
            }
        }
    }
}

